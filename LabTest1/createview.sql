CREATE VIEW StudentAdvise AS
SELECT S.StudentID, S.StudentName, S.StudentSurname , D.Deptname , A.AdvisorName , A.AdvisorSurname
FROM Student AS S
LEFT JOIN (Department AS D,Advisor As A)
ON (D.DeptID = A.DeptID AND A.AdvisorID = S.AdvisorID);
